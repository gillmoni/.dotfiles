"====[ Setting specific for log and pcap ]============================================
" if expand('%:e') == 'log' && expand('%:e') == 'pcap'  
	colorscheme nightshade
	
	" Making searching huge loh file easier. keep search at top
	xnoremap N NNztj
	xnoremap n nztj
	nnoremap N NNztj
	nnoremap n nztj

" This is just temporary keybindings to add PIMd status from homerow
"     nmap iaa A<C-R> [Consumer Start]<Esc>
"     nmap iss A<C-R>  [Consumer Stop]<Esc>
"     nmap iff A<C-R> [Producer Start]<Esc>
"     nmap idd A<C-R>  [Producer Stop]<Esc>
"     
"     imap iaa <Esc>A [Consumer Start] <Esc>i 
"     imap iss <Esc>A [Consumer Stop] <Esc>i 
"     imap iff <Esc>A [Producer Start] <Esc>i 
"     imap idd <Esc>A [Producer Stop] <Esc>i 
    
"     nmap <F1> A<C-R>  [Consumer Start]<Esc>
"     nmap <F2> A<C-R>  [Consumer Stop]<Esc>
"     nmap <F3> A<C-R>  [Producer Start]<Esc>
"     nmap <F4> A<C-R>  [Producer Stop]<Esc>
"     imap <F1> <Esc>A [Consumer Start] <Esc>i
"     imap <F2> <Esc>A [Consumer Stop] <Esc>i
"     imap <F3> <Esc>A [Producer Start] <Esc>i
"     imap <F4> <Esc>A [Producer Stop] <Esc>i


" endif

" nnoremap <Leader>d :colorscheme dual<CR>
" nnoremap <Leader>d :colorscheme eclipse<CR>
" nnoremap <Leader>d :colorscheme ego<CR>
" nnoremap <Leader>d :colorscheme eva01<CR>
" nnoremap <Leader>d :colorscheme flattr<CR>
" nnoremap <Leader>d :colorscheme gentooish<CR>
" nnoremap <Leader>d :colorscheme goodwolf<CR>
" nnoremap <Leader>d :colorscheme holokai<CR>
" nnoremap <Leader>d :colorscheme industrial<CR>
" nnoremap <Leader>d :colorscheme ir_black<CR>
nnoremap <Leader>df:colorscheme itg_flat<CR>
nnoremap <Leader>dd :colorscheme dracula<CR>
" nnoremap <Leader>nn :colorscheme nightshade<CR>


set cursorline     
