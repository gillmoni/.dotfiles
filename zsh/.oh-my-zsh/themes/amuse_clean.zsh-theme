# vim:ft=zsh ts=2 sw=2 sts=2
# user part, color coded by privileges
local user="%(!.%{$fg[white]%}.%{$fg[white]%})%n%{$reset_color%}"

# if not found, regular hostname in default color
local host="@${host_repr[$HOST]:-$HOST}%{$reset_color%}"

# Compacted $PWD
# local pwd="%{$fg[blue]%}%c%{$reset_color%}"
# local pwd="%{$fg_bold[yellow]%}%~%{$reset_color%}"
local pwd="%{$fg[yellow]%}%~%{$reset_color%}"

function venv_info {
  [ $VIRTUAL_ENV ] && echo "(`basename $VIRTUAL_ENV`)"
}
# PROMPT='${time} ${user}${host} ${pwd}'
# PROMPT='%{$fg_bold[green]%}%~%{$reset_color%} $ '
# PROMPT='${user}${host}${pwd} $ '
# PROMPT='[${user}${host} ${pwd}]$ '
# PROMPT='[${user}${host} ${pwd}]$ '

# Custom Prompt on next line.
PROMPT='%{$fg[cyan]%}╭─'
PROMPT+='%{$fg[yellow]%}$(venv_info)%{$reset_color%}'
PROMPT+='[${user}${host}] [${pwd}] [$(git_prompt_info)]
%{$fg[yellow]%}╰─>%B$ '
 
# Must use Powerline font, for \uE0A0 to render.
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[magenta]%}\uE0A0 "
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}!!"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[green]%}?"
ZSH_THEME_GIT_PROMPT_CLEAN=""


# elaborate exitcode on the right when >0
return_code_enabled="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"
return_code_disabled=
return_code=$return_code_enabled



function accept-line-or-clear-warning () {
	if [[ -z $BUFFER ]]; then
		time=$time_disabled
		return_code=$return_code_disabled
	else
		time=$time_enabled
		return_code=$return_code_enabled
	fi
	zle accept-line
}

zle -N accept-line-or-clear-warning
bindkey '^M' accept-line-or-clear-warning

if [ -e ~/.rvm/bin/rvm-prompt ]; then
  # RPS1='%{$fg_bold[green]%}$(git_prompt_info) [⌚%{$fg_bold[green]%}%*%{$reset_color%}]%{$reset_color%} ${return_code}'
  RPS1='%{$fg_bold[green]%}$(git_prompt_info) [⌚%{$fg[yellow]%}%*%{$reset_color%}]%{$reset_color%} ${return_code}'
else
  if which rbenv &> /dev/null; then
    # RPS1='%{$fg_bold[green]%$(git_prompt_info)}⌚%{$fg_bold[green]%}%*%{$reset_color%}%{$reset_color%} ${return_code}'
    RPS1='%{$fg_bold[green]%$(git_prompt_info)}⌚%{$fg[yellow]%}%*%{$reset_color%}%{$reset_color%} ${return_code}'
  fi
fi


RPS2='${return_code}'
