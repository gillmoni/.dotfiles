"====[ Setting specific for python ]============================================
"====[ Tab Handling ]============================================

set ts=4            " tab indentation levels every 4 columnskj
set softtabstop=4   " new
set shiftwidth=4    " does something?
set showmatch       " show matching braces
set textwidth=79    " pep-8 friendly kk
set cursorline      " how lline under active cursor
set fileformat=unix " keep python files as unix
set colorcolumn=102 " show a color in background at 102 char
setlocal expandtab

" set fo+=t

" Autohighlight when passing away more than [XXX] characters
highlight ColorColumn ctermfg=white
call matchadd('ColorColumn', '\%82v', 100)


" automatic syntax checking
" Plugin 'vim-syntastic/syntastic'

" pep 8 checking with vim
" Plugin 'nvie/vim-flake8'

" Flag un-necesary whitespaces
"  au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

 " Auto complete for vim
" Bundle 'Valloric/YouCompleteMe'
" let g:ycm_autoclose_preview_window_after_completion=1
" map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

"python with virtualenv support
" py << EOF
" import os
" import sys
" if 'VIRTUAL_ENV' in os.environ:
"   project_base_dir = os.environ['VIRTUAL_ENV']
"     activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"       execfile(activate_this, dict(__file__=activate_this))
"       EOF


let NERDTreeIgnore=['\.pyc$', '\~$', '__pyc*']     "ignore files in NERDTree



" Align Python
                " Left         Right        Insert
                " ==========   =====        =============================
call SmartcomAdd( ':',        ANYTHING,    AlignOnPat(':') )
call SmartcomAdd( ',',        ANYTHING,    AlignOnPat(',\s\W') )


" Python keywords...
                " Left             Right   Insert                                  Where
                " ==========       =====   =============================           ===================
call SmartcomAdd( '^\s*for',       EOL,    " ___ in (___):\n___",        {'filetype':'python'} )
call SmartcomAdd( '^\s*pri',       EOL,    "nt(f'___ {___}')",        {'filetype':'python'} )
call SmartcomAdd( '^\s*from',      EOL,    " ___ import ___",        {'filetype':'python'} )

augroup caucmds
    autocmd FileType python :iab  true   True
    autocmd FileType python :iab  false  False
augroup END


" Show only imports for Perl and Python
autocmd BufEnter *.py nmap <silent> <expr>  zu  FS_FoldAroundTarget('^\s*import\s\+\S.*',{'context':1})
autocmd BufEnter *.p[l|m] nmap <silent> <expr>  zu  FS_FoldAroundTarget('^\s*use\s\+\S.*;',{'context':1})


" Show only sub defns (and maybe comments)...
let py_sub_pat   = '^\s*\%(def\|class\|package\)\s\+\k\+'
augroup FoldSub
    autocmd!
    autocmd BufEnter *.py nmap <silent> <expr>  zp  FS_FoldAroundTarget(py_sub_pat,{'context':1})
    autocmd BufEnter *.py nmap <silent> <expr>  za  FS_FoldAroundTarget(py_sub_pat.'\\|^\s*".*',{'context':0, 'folds':'invisible'})
augroup END

" Smart completed for Python
iab  _nem  __name__ == "__main__"
iab  ipd  import pandas as pd

