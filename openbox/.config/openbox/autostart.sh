# Conky
conky -d 

# Kmix Audio
kmix &

#tint2
tint2 &

# Screensaver for lock app
xscreensaver -no-splash &

#Restore nitrogen wallpaper
#nitrogen --restore

#Mount Share(s) with GVFS
#/usr/lib/gvfs/gvfs-fuse-daemon ~/.gvfs

#terminator
terminator &

#Warning Gnome-Keyring
#eval $(gnome-keyring-daemon --start) &


feh --bg-fill ~/images/PixelMatrix-Grape_DeltaNine_1920X1200.png
