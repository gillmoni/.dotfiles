# Tmux config Moni Gill.
# Broadcast and easy resizing.
######################################
# remap prefix to Control + a
#bind 'C-a C-a' to type 'C-a'
bind C-a send-prefix
unbind C-b
set -g prefix C-a
# set -g prefix C-Space

# Mouse support - set to on if you want to use the mouse
set -g mouse on
#set -g mouse-select-pane on
#set -g mouse-resize-pane on
#set -g mouse-select-window on

# Set the default terminal mode to 256color mode #set -g default-terminal "screen-256color"

# Keys for x-term (coz shift wasn't being set properly)
set-option -gw xterm-keys on

# Unbind TAB key as it was giving some problem earlier
unbind-key Tab

# enable activity alerts
setw -g monitor-activity on
set -g visual-activity on

# enable pane number show time to last long
# set display-panes-time 2000

## Split panes H and V, with retaining the path
# v and h are not binded by default, but we never know in the next versions...
unbind v
unbind h
unbind % # Split vertically
unbind '"' # Split horizontally

bind v split-window -h -c "#{pane_current_path}"
bind h split-window -v -c "#{pane_current_path}"

# Broadcast all panes
bind g set synchronize-panes

# switch panes using Alt-arrow without prefix
# bind -n C-M-Left select-pane -L
# bind -n C-M-Right select-pane -R
# bind -n C-M-Up select-pane -U
# bind -n C-M-Down select-pane -D

# switch panes using Alt-arrow and vim style keybindings without prefix
# bind -n C-M-h select-pane -L
# bind -n C-M-j select-pane -U
# bind -n C-M-k select-pane -D
# bind -n C-M-l select-pane -R

# Improvement of above, where prefix key is not needed.
# this interferes with midnight commander custom vi keys
 bind -n M-h select-pane -L
 bind -n M-j select-pane -D
 bind -n M-k select-pane -U
 bind -n M-l select-pane -R

# Removed <Ctrl+L> coz it is interfering with Linux shortcut
unbind -n C-h
unbind -n C-j
unbind -n C-k
unbind -n C-l

# quickly go between session-windows using the alt key
# same as above, but uses Shift in combination to
# change session-window
bind -n M-J previous-window
bind -n M-K next-window

# reload config file (change file location to your the tmux.conf you want to use)
# unbind r
bind R source-file ~/.tmux.conf \; display "Reloaded ~/.tmux.conf"

# Creting new window with same path
unbind c
bind c new-window -c "#{pane_current_path}"

# Merge a pane back into a window
bind-key j command-prompt -p "join pane from: "  "join-pane -h -s '%%'"

#Tmux resize panes quickly
bind -n S-Up resize-pane -U 2
bind -n S-Down resize-pane -D 2
bind -n S-Left resize-pane -L 2
bind -n S-Right resize-pane -R 2

#Tmux resize panes quickly with vi keys
# bind -n C-S-k resize-pane -U 2
# bind -n C-S-j resize-pane -D 2
# bind -n C-S-h resize-pane -L 2
# bind -n C-S-l resize-pane -R 2

#Select windows quickly with alt+numbers
#bind-key -n M-` select-window -t 0
bind-key -n M-w last-window 
bind-key -n M-` last-pane 
bind-key -n M-0 select-window -t 0
bind-key -n M-1 select-window -t 1
bind-key -n M-2 select-window -t 2
bind-key -n M-3 select-window -t 3
bind-key -n M-4 select-window -t 4
bind-key -n M-5 select-window -t 5
bind-key -n M-6 select-window -t 6
bind-key -n M-7 select-window -t 7
bind-key -n M-8 select-window -t 8
bind-key -n M-9 select-window -t 9

# jump directly between tmux-panes
# this only work if you set proper
# escape sequence with terminal settings
# bind-key -n C-0 select-pane -t 0
# bind-key -n C-1 select-pane -t 1
# bind-key -n C-2 select-pane -t 2
# bind-key -n C-3 select-pane -t 3
# bind-key -n C-4 select-pane -t 4
# bind-key -n C-5 select-pane -t 5
# bind-key -n C-6 select-pane -t 6
# bind-key -n C-7 select-pane -t 7
# bind-key -n C-8 select-pane -t 8
# bind-key -n C-9 select-pane -t 9

# Set vi keys in tmux copy mode
set-window-option -g mode-keys vi
bind y run-shell "tmux show-buffer | xclip -sel clip -i" \; display-message "Copied tmux buffer to system clipboard"

#Make it pretty
#  modes
#setw -g clock-mode-colour colour1
#setw -g mode-attr bold
#setw -g mode-bg colour0
#setw -g mode-fg colour1
#
# panes
#set -g pane-border-bg colour0
#set -g pane-border-fg colour1
#set -g pane-active-border-bg colour0
#set -g pane-active-border-fg colour1
#


######################
### LAYOUT PREDEF  ###
######################
# Note: spaces, comments, newlines aren't allowed
bind-key M-x kill-window -a -t 0
 
# below creates my typical layout of all windows and panes
# leave current pane and create new panes
bind-key M-f new-window -n dash -c $PWD \; \
    send-keys 'clear' 'Enter' \; \
    split-window -v -p 80 -t 0 \; \
    split-window -h -p 30 -t 0 \; \
    split-window -v -p 30 -t 2 \; \
    split-window -h -p 50 -t 3 \; \
    select-pane -t 0 \; \
    send-keys 'htop' 'Enter' \; \
    select-pane -t 2 \; \
    new-window -n dev -c $PWD \; \
    send-keys -t dev 'clear' 'Enter' \; \
    split-window -v -p 30 -t 0 \; \
    split-window -h -p 50 -t 1 \; \
    select-pane -t 0 \; \
    new-window -n log -c $PWD \; \
    send-keys -t log 'clear' 'Enter' \; \
    split-window -v -p 30 -t 0 \; \
    select-pane -t 0 \; \
    new-window -n some -c $PWD \; \
    send-keys -t some 'clear' 'Enter' \; \
    split-window -v -p 30 -t 0 \; \
    split-window -h -p 50 -t 1 \; \
    new-window -n logs -c $PWD \; \
    send-keys -t logs 'ranger' 'Enter' \; \
    split-window -v -p 30 -t 0 \; \
    select-pane -t 0\; \
    new-window -n web -c $PWD \; \
    send-keys -t web 'clear' 'Enter' \; \
    split-window -v -p 50 -t 0 \; \
    send-keys -t web 'clear' 'Enter' \; \


# rename current pane and create new panes
bind-key M-d rename-window 'dash' \; \
    send-keys 'clear' 'Enter' \; \
    split-window -v -p 80 -t 0 \; \
    split-window -h -p 30 -t 0 \; \
    split-window -v -p 30 -t 2 \; \
    split-window -h -p 50 -t 3 \; \
    select-pane -t 0 \; \
    send-keys 'htop' 'Enter' \; \
    select-pane -t 2 \; \
    new-window -n dev -c $PWD \; \
    send-keys -t dev 'clear' 'Enter' \; \
    split-window -v -p 30 -t 0 \; \
    split-window -h -p 50 -t 1 \; \
    select-pane -t 0 \; \
    new-window -n log -c $PWD \; \
    send-keys -t log 'clear' 'Enter' \; \
    split-window -v -p 30 -t 0 \; \
    select-pane -t 0 \; \
    new-window -n some -c $PWD \; \
    send-keys -t some 'clear' 'Enter' \; \
    split-window -v -p 30 -t 0 \; \
    split-window -h -p 50 -t 1 \; \
    new-window -n logs -c $PWD \; \
    send-keys -t logs 'ranger' 'Enter' \; \
    split-window -v -p 30 -t 0 \; \
    select-pane -t 0\; \
    new-window -n web -c $PWD \; \
    send-keys -t web 'clear' 'Enter' \; \
    split-window -v -p 50 -t 0 \; \
    send-keys -t web 'clear' 'Enter' \; \

    # detach

######################
### DESIGN CHANGES ###
######################

#  modes
setw -g clock-mode-colour colour5
# setw -g mode-style 'fg=colour1 bg=colour18'

# panes
set -g pane-border-style 'fg=colour19 bg=colour0'
set -g pane-active-border-style 'bg=colour0 fg=colour2'

# statusbar
set -g status-position top
set -g status-justify left
set -g status-left 'tmux'
set -g status-style 'bg=colour233 fg=colour252'

# Show session-name in status bar
set-window-option -g status-left "@#S "

# clock
set -g status-right '#[fg=colour252,bg=colour57] %d/%m #[fg=colour252,bg=colour57] %H:%M:%S'
set -g status-right-length 50
set -g status-left-length 20

# active tab (window-name)
setw -g window-status-current-style 'fg=colour252 bg=colour57'
setw -g window-status-current-format ' #I#[fg=colour252]:#[fg=colour252]#W#[fg=colour252]#F'

# inactive tab (window-name)
setw -g window-status-style 'fg=colour57 bg=colour232'
setw -g window-status-format ' #I|#[fg=colour57]#[fg=colour57]#W#[fg=colour57] '

setw -g window-status-bell-style 'fg=colour25 bg=colour1 bold'

# status messages (color and background)
set -g message-style 'fg=colour252 bg=colour57 bold'

# do not automatically rename the session-windows
# set allow-rename off
setw -g automatic-rename off
set-window-option -g automatic-rename off

# Console logging options: test
bind-key L pipe-pane "exec cat >> $HOME/my#W-tmux.log" \; display-message "Started logging to $HOME/#W-tmux.log" 
bind-key K pipe-pane \; display-message "Ended logging to $HOME/#W-tmux.log" 

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

# Tmux logging plugin
set -g @plugin 'tmux-plugins/tmux-logging'

# Copy plugins
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-open'
set -g @yank_with_mouse on

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com/user/plugin'
# set -g @plugin 'git@bitbucket.com/user/plugin'

set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @continuum-save-interval '1'
set -g status-right 'S[#{continuum_status}]'
set -g @continuum-restore 'on'


# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'

