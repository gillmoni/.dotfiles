" vim-sublime - A minimal Sublime Text - like vim experience 
" http://github.com/grigio/vim-sublime
" Best view with a 256 color terminal and Powerline fonts
" Updated by Dorian Neto (https://github.com/dorianneto)"

" so ~/.vim/plugins.vim "

"Tell vim that my terminal emulator can have 256 colors"
set t_Co=256
set mouse=a

set ruler
"Search down into subfolders.
"Provide tab completion for all file-related tasks.
set path+=**
set nohls
:nnoremap <silent><expr> <S-h> (&hls && v:hlsearch ? ':nohls' : ':set hls')."\n"

"Create the 'tags' file
command! MakeTags !ctags -R .

"Set relative numbers
set relativenumber

set nocompatible
set nowrap
set autoread
filetype on


"Quick navigate between tabs with leader key
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>

"Set quick zooming of vim splits
noremap <Leader>z <c-w>_ \| <c-w>\|
noremap <Leader><Leader>z <c-w> = 
"nnoremap <silent> <Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
"nnoremap <silent> <Leader>- :exe "resize " . (winheight(0) * 2/3)<CR>

" Quickly jump between splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Quickly resize vertical buffer
nnoremap <C-w>. :vertical:resize -10<CR>
nnoremap <C-w>, :vertical:resize +10<CR>
nnoremap <C-w>> :resize +10<CR>
nnoremap <C-w>- :resize -10<CR>

set rtp+=~/.vim/bundle/vundle
call vundle#rc()

" This is the Vundle package, which can be found on GitHub.
" For GitHub repos, you specify plugins using the
" 'user/repository' format"
Plugin 'gmarik/vundle'
Plugin 'emilyst/match-count-statusline'

" Add plugin for cisco config highlight
Plugin 'momota/cisco.vim'

" We could also add repositories with a ".git" extension"
Plugin 'scrooloose/nerdtree.git'
Plugin 'francoiscabrol/ranger.vim'

" To get plugins from Vim Scripts, you can reference the plugin
" by name as it appears on the site
Plugin 'Buffergator'

Plugin 'catalinciurea/perl-nextmethod'
"Plugin 'altercation/vim-colors-solarized'"
Plugin 'airblade/vim-gitgutter'

nmap ghp <Plug>(GitGutterPreviewHunk)
nmap ghs <Plug>(GitGutterStageHunk)
nmap ghu <Plug>(GitGutterUndoHunk)

"Plugin 'valloric/youcompleteme'"
" Plugin 'vim-airline/vim-airline-themes'
Plugin 'mileszs/ack.vim'
Plugin 'junegunn/fzf'
Plugin 'easymotion/vim-easymotion'

" Vim fugitive for git
Plugin 'tpope/vim-fugitive'
nmap<leader>gs :G<CR>
nmap<leader>gj :diffet //3<CR>
nmap<leader>gf :diffet //2<CR>

" Quickly do the replace work under cursor (in-file)
" nnoremap<leader>s :%s/\<<C-r><C-w>\>/<CR>

Plugin 'tpope/vim-eunuch'
Plugin 'tpope/vim-surround'
Plugin 'dhruvasagar/vim-zoom'
Plugin 'dhruvasagar/vim-table-mode'
nmap <Leader>tt :TableModeToggle<CR>


Plugin 'fisadev/vim-ctrlp-cmdpalette'
Plugin 'terryma/vim-multiple-cursors'
"let g:multi_cursor_use_default_mapping=0"

Plugin 'itchyny/lightline.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'tomtom/tcomment_vim'
Plugin 'severin-lemaignan/vim-minimap'
"Plugin 'ap/vim-buftabline'"

Plugin 'mkitt/tabline.vim'
let g:tablineclosebutton=1

"Plugin 'ctags.vim'"
Plugin 'craigemery/vim-autotag'
Plugin 'taglist.vim'

"Show current function in status line
" set statusline += \ %{Tlist_Get_Tagname_By_Line()}
set statusline+=%{zoom#statusline()}


Plugin 'erikw/tmux-powerline'
"Plugin 'molok/vim-smartusline'"


hi TabLine      ctermfg=Black  ctermbg=Green     cterm=NONE
hi TabLineFill  ctermfg=Black  ctermbg=Green     cterm=NONE
hi TabLineSel   ctermfg=White  ctermbg=DarkBlue  cterm=NONE
let g:tablineclosebutton=1

"modify close buffer without affecting panes
command! Bd bp\|bd \#

Plugin 'majutsushi/tagbar'
" Plugin 'bling/vim-airline'
" Plugin 'osyo-manga/vim-anzu'
" let g:airline#extensions#tagbar#flags = 'f'  " show full tag hierarchy"

"Plugin 'AnsiEsc.vim'"
"Plugin 'Improved-AnsiEsc'"
Plugin 'int3/vim-extradite'
Plugin 'skywind3000/vim-preview'


"Jump between multiple tabs"
"nnoremap <C-Left> :tabprevious<CR>                                                                            
" nnoremap <C-j> :tabprevious<CR>                                                                            
"nnoremap <C-Right> :tabnext<CR>
" nnoremap <C-k> :tabnext<CR>

"Jump between multiple buffers"
" nnoremap <C-j> :bprev<CR>                                                                            
" nnoremap <C-k> :bnext<CR>

" Now we can turn our filetype functionality back on"
filetype plugin indent on


if has('syntax') && !exists('g:syntax_on')
  syntax enable
endif


" Enable folding "
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar "
nnoremap <space> za
vnoremap <space> zf
Plugin 'tmhedberg/simpylfold'
set foldmethod=indent

set splitbelow  " horziontal split below
set splitright " verical split to the right side

set number  " show numbers on lines
set hlsearch " highlight searched item
set ignorecase " search should ignore casing
set smartcase  " enable smartcasing


"=====[ Sublime Text like bindings ]======================

"Make line moving up/down easy"
" noremap <c-s-j> :m -2<CR>
" noremap <c-s-k> :m +1<CR>

" map <C-z> :undo<CR>
" map <C-y> :redo<CR>
" map <C-r> :CtrlPBufTag<CR>
" map <C-r> :TagbarToggle<CR>"
" map <C-e> :tabclose<CR>
" map <C-S-d> :y<CR> <bar> p<CR>
" map <C-f> /

map <C-f> :TagbarToggle<CR>
map <C-s> :write
map <C-t> :tabe<CR>
" map <C-x> :delete<CR>
map <C-S-p> :sh<CR>
map <C-m> %
map <silent> <C-k>b :NERDTreeToggle<CR>

" map <C-u> :vnew<CR>
" nnoremap <C-k>k :only<bar>vsplit<CR>:execute ":PreviewTag " . expand('<cword>')<CR>"

" Making Vim CtrlP load time100x faster
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']



"=====[ Enhances for plugin installation  ]======================

"map <F8> <ESC>:echo 'test' <bar> :echo 'test2' <bar>:echo 'test3'<CR>"
"map <F9>  <ESC>:source ~/.vimrc <bar><ESC>:PluginClean!   <CR>
"map <F8> <ESC>:source ~/.vimrc <bar><ESC>:PluginInstall! <CR>
"<bar>:PluginInstall  <CR> <bar> :bdelete <CR>"


"=====[ Plugins from Damian Conway ]======================

Plugin 'snaewe/instantly_better_vim_2013'
Plugin 'jamshedvesuna/vim-markdown-preview'
let vim_markdown_preview_browser='Google Chrome'

"=====[ .t files are perl ]======================

autocmd BufNewFile,BufRead  *.t                     setfiletype perl


"=====[ Comments are important ]==================

highlight Comment term=bold ctermfg=white


"======[ Magically build interim directories if necessary ]===================

function! AskQuit (msg, options, quit_option)
    if confirm(a:msg, a:options) == a:quit_option
        exit
    endif
endfunction

function! EnsureDirExists ()
    let required_dir = expand("%:h")
    if !isdirectory(required_dir)
        call AskQuit("Parent directory '" . required_dir . "' doesn't exist.",
             \       "&Create it\nor &Quit?", 2)

        try
            call mkdir( required_dir, 'p' )
        catch
            call AskQuit("Can't create '" . required_dir . "'",
            \            "&Quit\nor &Continue anyway?", 1)
        endtry
    endif
endfunction

augroup AutoMkdir
    autocmd!
    autocmd  BufNewFile  *  :call EnsureDirExists()
augroup END


"=====[ Make arrow keys move visual blocks around ]======================

vmap <up>    <Plug>SchleppUp
vmap <down>  <Plug>SchleppDown
vmap <left>  <Plug>SchleppLeft
vmap <right> <Plug>SchleppRight

vmap D       <Plug>SchleppDupLeft
vmap <C-D>   <Plug>SchleppDupLeft


"=====[ Configure % key (via matchit plugin) for Perl 6 as well ]=============

" Match angle brackets...
set matchpairs+=<:>,«:»


" =====[ Perl programming support ]===========================

"Adjust keyword characters to match Perlish identifiers...
set iskeyword+=$
set iskeyword+=%
set iskeyword+=@-@
set iskeyword+=:
set iskeyword-=,


" =====[ Insert common Perl code structures ]===========================
 
" Code completion for ruggedtest variables
iab rli  $logger->log_info("
iab rlc  $logger->log_comment("
iab rlde $logger->log_description("
iab rldo $logger->log_done_step("
iab rldo $logger->log_done_step("
iab LL ln:[" . __LINE__  . "]
iab FF ln:[" . __FILE__  . "]
iab wsc $sub_caller():
iab dsc my $sub_caller = substr((caller(0))[3], 6);


" iab udd use Data::Dump::Simple 'ddx';<CR>ddx;<LEFT>
iab udd use Data::Dumper::Simple; <CR><Esc>x
iab udv use Dumpvalue;<CR>Dumpvalue->new->dumpValues();<ESC>hi
iab uds use Data::Show;<CR>show
iab ubm use Benchmark qw( cmpthese );<CR><CR>cmpthese -10, {<CR>};<ESC>O
iab usc use Smart::Comments;<CR>###
iab uts use Test::Piece;
iab uts use Test::Simple 'no_plan';
iab utm use Test::More 'no_plan';
iab dbs $DB::single = 1;<ESC>


"=====[ Run a Perl module's test suite ]=========================

let g:PerlTests_program       = 'perltests'   " ...What to call
let g:PerlTests_search_height = 5             " ...How far up to search
let g:PerlTests_test_dir      = '/t'          " ...Where to look for tests

augroup Perl_Tests
    autocmd!
    autocmd BufEnter *.p[lm]  nmap <buffer> ;t :call RunPerlTests()<CR>
    autocmd BufEnter *.t      nmap <buffer> ;t :call RunPerlTests()<CR>
augroup END

function! RunPerlTests ()
    " Start in the current directory...
    let dir = expand('%:h')

    " Walk up through parent directories, looking for a test directory...
    for n in range(g:PerlTests_search_height)
        " When found...
        if isdirectory(dir . g:PerlTests_test_dir)
            " Go there...
            silent exec 'cd ' . dir

            " Run the tests...
            exec ':!' . g:PerlTests_program

            " Return to the previous directory...
            silent cd -
            return
        endif

        " Otherwise, keep looking up the directory tree...
        let dir = dir . '/..'
    endfor

    " If not found, report the failure...
    echohl WarningMsg
    echomsg "Couldn't find a suitable" g:PerlTests_test_dir '(tried' g:PerlTests_search_height 'levels up)'
    echohl None
endfunction


"=====[ Auto-setup for Perl scripts and modules and test files ]===========

augroup Perl_Setup
    autocmd!
    autocmd BufNewFile   *.p[lm65],*.t   0r !perl_file_template <afile>
    autocmd BufNewFile   *.p[lm65],*.t   /^[ \t]*[#].*implementation[ \t]\+here/
augroup END


"=====[ Proper syntax highlighting for Rakudo files ]===========
autocmd BufNewFile,BufRead  *   :call CheckForPerl6()

function! CheckForPerl6 ()
    if getline(1) =~ 'rakudo'
        setfiletype perl6
    endif
    if expand('<afile>:e') == 'pod6'
        highlight Pod6Block_Heading1 cterm=bold,underline
        highlight Pod6FC_Important cterm=underline

        setfiletype pod6
        syntax enable
    endif
endfunction

" =====[ Smart completion via <TAB> and <S-TAB> ]=============
runtime plugin/smartcom.vim


" Add extra completions (mainly for Perl programming)...
let ANYTHING = ""
let NOTHING  = ""
let EOL      = '\s*$'

                " Left     Right      Insert                             Reset cursor
                " =====    =====      ===============================    ============
call SmartcomAdd( '<<',    ANYTHING,  '>>',                              {'restore':1} )
call SmartcomAdd( '<<',    '>>',      "\<CR>\<ESC>O\<TAB>"                             )
call SmartcomAdd( '?',     ANYTHING,  '?',                               {'restore':1} )
call SmartcomAdd( '?',     '?',       "\<CR>\<ESC>O\<TAB>"                             )
call SmartcomAdd( '{{',    ANYTHING,  '}}',                              {'restore':1} )
call SmartcomAdd( '{{',    '}}',      NOTHING,                                         )
call SmartcomAdd( 'qr{',   ANYTHING,  '}xms',                            {'restore':1} )
call SmartcomAdd( 'qr{',   '}xms',    "\<CR>\<C-D>\<ESC>O\<C-D>\<TAB>"                 )
call SmartcomAdd( 'm{',    ANYTHING,  '}xms',                            {'restore':1} )
call SmartcomAdd( 'm{',    '}xms',    "\<CR>\<C-D>\<ESC>O\<C-D>\<TAB>",                )
call SmartcomAdd( 's{',    ANYTHING,  '}{}xms',                          {'restore':1} )
call SmartcomAdd( 's{',    '}{}xms',  "\<CR>\<C-D>\<ESC>O\<C-D>\<TAB>",                )
call SmartcomAdd( '\*\*',  ANYTHING,  '**',                              {'restore':1} )
call SmartcomAdd( '\*\*',  '\*\*',    NOTHING,                                         )

" Handle single : correctly...
call SmartcomAdd( '^:\|[^:]:',  EOL,  "\<TAB>" )

"After an alignable, align...
function! AlignOnPat (pat)
    return "\<ESC>:call EQAS_Align('nmap',{'pattern':'" . a:pat . "'})\<CR>A"
endfunction
                " Left         Right        Insert
                " ==========   =====        =============================
call SmartcomAdd( '=',         ANYTHING,    "\<ESC>:call EQAS_Align('nmap')\<CR>A")
call SmartcomAdd( '=>',        ANYTHING,    AlignOnPat('=>') )
call SmartcomAdd( '\s#',       ANYTHING,    AlignOnPat('\%(\S\s*\)\@<= #') )
call SmartcomAdd( '[''"]\s*:', ANYTHING,    AlignOnPat(':'),                   {'filetype':'vim'} )
call SmartcomAdd( ':',         ANYTHING,    "\<TAB>",                          {'filetype':'vim'} )
call SmartcomAdd( '"',         ANYTHING,    AlignOnPat('"'),                   {'filetype':'vim'} )

" Align real-t.run file params
                " Left         Right        Insert
                " ==========   =====        =============================
"call SmartcomAdd( '\*[0-9]+',  ANYTHING,    AlignOnPat('\*[0-9]+') )
call SmartcomAdd( '\*1',       ANYTHING,    AlignOnPat('\*1') )
call SmartcomAdd( '-d',        ANYTHING,    AlignOnPat('-d') )
call SmartcomAdd( '-d',        ANYTHING,    AlignOnPat('-d') )
call SmartcomAdd( '-l',        ANYTHING,    AlignOnPat('-l') )
call SmartcomAdd( '-u',        ANYTHING,    AlignOnPat('-u') )
call SmartcomAdd( '-r',        ANYTHING,    AlignOnPat('-r') )
call SmartcomAdd( '#/',        ANYTHING,    AlignOnPat('#/') )


" Perl keywords...
                " Left             Right   Insert                                  Where
                " ==========       =====   =============================           ===================
call SmartcomAdd( '^\s*for',       EOL,    " my $___ (___) {\n___\n}\n___",        {'filetype':'perl'} )
call SmartcomAdd( '^\s*if',        EOL,    " (___) {\n___\n}\n___",                {'filetype':'perl'} )
call SmartcomAdd( '^\s*while',     EOL,    " (___) {\n___\n}\n___",                {'filetype':'perl'} )
call SmartcomAdd( '^\s*given',     EOL,    " (___) {\n___\n}\n___",                {'filetype':'perl'} )
call SmartcomAdd( '^\s*when',      EOL,    " (___) {\n___\n}\n___",                {'filetype':'perl'} )

" call SmartcomAdd( '"\s+\.\sDu',    EOL,    "mper($___, $___));",                   {'filetype':'perl'} )
" call SmartcomAdd( '^\s+*bli',      EOL,    " $logger->log_info(\"___\");",         {'filetype':'perl'} )
" call SmartcomAdd( '^\s+*blc',      EOL,    " $logger->log_comment(\"___\");",      {'filetype':'perl'} )
" call SmartcomAdd( '^\s+*blde',     EOL,    " $logger->log_description(\"___\");",  {'filetype':'perl'} )
" call SmartcomAdd( '^\s+*bldo',     EOL,    " $logger->log_done_step(\"___\");",    {'filetype':'perl'} )
" call SmartcomAdd( '^\s+*bldo',     EOL,    " $logger->log_done_step(\"___\");",     {'filetype':'perl'} )

"=====[ Correct common mistypings in-the-fly ]=======================

iab        ,,  =>
iab    retrun  return
iab     pritn  print
iab       teh  the
iab      liek  like
iab  liekwise  likewise
iab      Pelr  Perl
iab      pelr  perl
iab        ;t  't
iab    Jarrko  Jarkko
iab    jarrko  jarkko
iab      moer  more
iab  previosu  previous
iab  =+        +=
iab  =-        -=

" Do these changes only for Perl files 
filetype plugin indent on
augroup caucmds
    autocmd!
    autocmd FileType perl :iab nmatches !~
    autocmd FileType perl :iab matches  =~
    autocmd FileType perl :iab contains =~
    autocmd FileType perl :iab has      =~
    autocmd FileType perl :iab iz       == 
    autocmd FileType perl :iab isnot    !=
augroup END

"=====[ Add or subtract comments ]===============================

" Work out what the comment character is, by filetype...
autocmd FileType             *sh,awk,python,perl,perl6,ruby    let b:cmt = exists('b:cmt') ? b:cmt : '#'
autocmd FileType             vim                               let b:cmt = exists('b:cmt') ? b:cmt : '"'
autocmd BufNewFile,BufRead   *.vim,.vimrc                      let b:cmt = exists('b:cmt') ? b:cmt : '"'
autocmd BufNewFile,BufRead   *                                 let b:cmt = exists('b:cmt') ? b:cmt : '#'
autocmd BufNewFile,BufRead   *.p[lm],.t                        let b:cmt = exists('b:cmt') ? b:cmt : '#'
autocmd BufNewFile,BufRead   *.txt                             let b:cmt = exists('b:cmt') ? b:cmt : '#'

" Work out whether the line has a comment then reverse that condition...
function! ToggleComment ()
    " What's the comment character???
    let comment_char = exists('b:cmt') ? b:cmt : '#'

    " Grab the line and work out whether it's commented...
    let currline = getline(".")

    " If so, remove it and rewrite the line...
    if currline =~ '^' . comment_char
        let repline = substitute(currline, '^' . comment_char . " ", "", "")
        call setline(".", repline)

    " Otherwise, insert it...
    else
        let repline = substitute(currline, '^' ,  comment_char . " ", "")
        call setline(".", repline)
    endif
endfunction

" Toggle comments down an entire visual selection of lines...
function! ToggleBlock () range
    " What's the comment character???
    let comment_char = exists('b:cmt') ? b:cmt : '#'

    " Start at the first line...
    let linenum = a:firstline

    " Get all the lines, and decide their comment state by examining the first...
    let currline = getline(a:firstline, a:lastline)
    if currline[0] =~ '^' . comment_char
        " If the first line is commented, decomment all...
        for line in currline
            let repline = substitute(line, '^' . comment_char, "", "")
            call setline(linenum, repline)
            let linenum += 1
        endfor
    else
        " Otherwise, encomment all...
        for line in currline
            let repline = substitute(line, '^\('. comment_char . '\)\?', comment_char, "")
            call setline(linenum, repline)
            let linenum += 1
        endfor
    endif
endfunction

" Set up the relevant mappings 
" Since i don't use Ex mode, Q seems better
nmap <silent> Q :call ToggleComment()<CR>j0
vmap <silent> Q :call ToggleBlock()<CR>


"=====[ Search folding ]=====================

" Don't start new buffers folded
set foldlevelstart=99

" Highlight folds
highlight Folded  ctermfg=cyan ctermbg=black

" Toggle folds on and off...
nmap <silent> <expr>  zz   FS_ToggleFoldAroundSearch({'context':0, 'folds':'invisible'})
nmap <silent> <expr>  zzz  FS_ToggleFoldAroundSearch({'context':1})
nmap <silent> <expr>  zx   FS_ToggleFoldAroundSearch({'context':2, 'folds':'visible'})
nmap <silent> <expr>  zxx  FS_ToggleFoldAroundSearch({'context':4, 'folds':'visible'})
nmap <silent> <expr>  zj  FS_ToggleFoldAroundSearch({'context':8, 'folds':'visible'})

" Show only sub defns (and maybe comments)...
let perl_sub_pat = '^\s*\%(sub\|func\|method\|package\)\s\+\k\+\|info.txt\|describe\|done_step'
let vim_sub_pat  = '^\s*fu\%[nction!]\s\+\k\+'

augroup FoldSub
    autocmd!
    autocmd BufEnter *.pl nmap <silent> <expr>  zp  FS_FoldAroundTarget(perl_sub_pat,{'context':1})
    autocmd BufEnter *.pl nmap <silent> <expr>  za  FS_FoldAroundTarget(perl_sub_pat.'\zs\\|^\s*#.*',{'context':0, 'folds':'invisible'})

    autocmd BufEnter *.vim,.vimrc nmap <silent> <expr>  zp  FS_FoldAroundTarget(vim_sub_pat,{'context':1})
    autocmd BufEnter *.vim,.vimrc nmap <silent> <expr>  za  FS_FoldAroundTarget(vim_sub_pat.'\\|^\s*".*',{'context':0, 'folds':'invisible'})
    autocmd BufEnter * nmap <silent> <expr>             zv  FS_FoldAroundTarget(vim_sub_pat.'\\|^\s*".*',{'context':0, 'folds':'invisible'})
augroup END


"=====[ Much smarter "edit next file" command ]=======================
" nmap <silent><expr>  e  g:TF_goto_file()
nmap <silent><expr>  q  g:GTF_goto_file('`')



"=====[ Smarter interstitial completions of identifiers ]=============
" When autocompleting within an identifier, prevent duplications...

augroup Undouble_Completions
    autocmd!
    autocmd CompleteDone *  call Undouble_Completions()
augroup None

function! Undouble_Completions ()
    let col  = getpos('.')[2]
    let line = getline('.')
    call setline('.', substitute(line, '\(\k\+\)\%'.col.'c\zs\1', '', ''))
endfunction


"====[ Escape insert mode via 'jj' ]=============================
imap jj <ESC>

"====[ :wm as save and !make via ':wm' ]=============================
" :imap <F10> <Esc>:w<CR>:!make<CR>a
" :map <F10> <Esc>:w<CR>:!make<CR>a


set autowrite       "Save buffer automatically when changing files
set autoread        "Always reload buffer when external changes detected

" check one time after 4s of inactivity in normal mode                                                                                                                                                                 
au CursorHold * checktime

set title           "Show filename in titlebar of window
set titleold=
set title titlestring=
"set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:~:.:h\")})%)%(\ %a%)

" Shift-Tab in visual mode to number lines...
xnoremap <S-TAB> :s/\%V/0<C-V><TAB>/<CR>gvg<C-A>gv:retab<ESC>gvI<C-G>u<ESC>gv/ <CR>:s/\%V /./<CR>


" Use space to jump down a page (like browsers do)...
" nnoremap   <Shift><Space> <PageDown>
" xnoremap   <Shift><Space> <PageDown>


" Indent/outdent current block...
nmap %% $>i}``
nmap $$ $<i}``

" use 00 to jump to end of line
nmap 00 $



"=====[ Correct common mistypings in-the-fly ]=======================

iab    retrun  return
iab     pritn  print
iab       teh  the
iab      liek  like
iab  liekwise  likewise
iab      Pelr  Perl
iab      pelr  perl
iab        ;t  't
iab    Jarrko  Jarkko
iab    jarrko  jarkko
iab      moer  more
iab  previosu  previous
iab  descrive  describe


"=====[ Tab handling ]======================================

set tabstop=4      "Tab indentation levels every four columns
set shiftwidth=4   "Indent/outdent by four columns
set expandtab      "Convert all tabs that are typed into spaces
set shiftround     "Always indent/outdent to nearest tabstop
set smarttab       "Use shiftwidths at left margin, tabstops everywhere else


" Make the completion popup look menu-ish on a Mac...
highlight  Pmenu        ctermbg=white   ctermfg=black
highlight  PmenuSel     ctermbg=blue    ctermfg=white   cterm=bold
highlight  PmenuSbar    ctermbg=grey    ctermfg=grey
highlight  PmenuThumb   ctermbg=blue    ctermfg=blue

" Make diffs less glaringly ugly...
highlight DiffAdd     cterm=bold ctermfg=green     ctermbg=black
highlight DiffChange  cterm=bold ctermfg=grey      ctermbg=black
highlight DiffDelete  cterm=bold ctermfg=black     ctermbg=black
highlight DiffText    cterm=bold ctermfg=magenta   ctermbg=black


"=======[ Fix smartindent stupidities ]============

set autoindent                              "Retain indentation on next line
set smartindent                             "Turn on autoindenting of blocks
let g:vim_indent_cont = 0                   " No magic shifts on Vim line continuations

"And no shift magic on comments...
nmap <silent>  >>  <Plug>ShiftLine
nnoremap <Plug>ShiftLine :call ShiftLine()<CR>
function! ShiftLine() range
    set nosmartindent
    exec "normal! " . v:count . ">>"
    set smartindent
    silent! call repeat#set( "\<Plug>ShiftLine" )
endfunction


"=====[ Key timeout values for responsivenes ]========================
set timeout timeoutlen=300 ttimeoutlen=300 " Keycodes and maps timeout in 3/10 sec...
set updatetime=2000 				" "idleness" is 2 sec
set scrolloff=3                     "Scroll when 3 lines from top/bottom


"====[ Simplify textfile backups ]============================================

" Back up the current file
" Nmap BB [Back up current file]  :!bak -q %<CR><CR>:echomsg "Backed up" expand('%')<CR>


"=====[ Remap various keys to something more useful ]========================

"Map to display the list of buffers
" :nnoremap <F7> 5l

"====[ Mapping of semi-colon ]============================================
map ; :
noremap ;; ;

" Awesome vim colorschemes
"Plugin 'rafi/awesome-vim-colorschemes'
" Good solarized color scheme
Plugin 'altercation/solarized'


"====[ Vim Colorschemes/Material ]============================================
colorscheme dracula
set background=dark
syntax enable


" Use a different colorscheme for vimdiff
if &diff
    " colorscheme solarized
    set syntax=off
endif



" Add a timestamp in file
"nmap <leader>now i<C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><Esc>2o
"imap <leader>now i<C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR>2o


" Open FireFox and Google Chrome quick
":map <leader>wg :!google-chrome-stable --auto-detect-proxy & <CR>
":map <leader>wf :!firefox & <CR>
":map <leader>rr :!ranger <CR>


"====[ TPOPE Plugins ]============================================

"====[ Organization/Wiki plugings ]============================================

" Use one of these Vimwiki or Orgmode
" VimWiki
Plugin 'vimwiki/vimwiki'
let g:Vimwiki_LISt = [{'auto_diary_index':1}]

" Org mode style for vim
Plugin 'dhruvasagar/vim-dotoo'

" These were checked but are unmaintained
"Plugin 'jceb/vim-orgmode'
"Plugin 'ichyny/calendar.vim'

"====[ Add PDF file functionality ]============================================

Plugin 'makerj/vim-pdf'
Plugin 'archernar/vim-session'


"====[ Make color switching easy ]============================================

Plugin 'xolox/vim-colorscheme-switcher'
Plugin 'xolox/vim-misc'
nnoremap <F9> :NextColorScheme<CR>
nnoremap <F8> :PrevColorScheme<CR>
nnoremap <Leader>c :set syntax=cisco<CR>

au BufRead,BufNewFile *.log set filetype=log
au BufRead,BufNewFile *.cfg set filetype=log
au BufRead,BufNewFile *.run set filetype=log
au BufRead,BufNewFile *.pcap set filetype=capture


" This is temporary to work with
" TODO saving buffer issue until i troubleshoot it
:set buftype=""
noremap <Leader>we :set buftype=<CR>  :w<CR>
command! W set buftype= | :w

" :map <F2> :ls<CR>
imap <F2> <C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR>
nmap <F2> i<C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><Esc>o<Esc>
map  <F3> :TagbarToggle<CR>"
nmap <F5> :source ~/.vimrc<CR>

" Insert now time in INSERT mode
iab :dt <C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR> 

" Insert current timestamp while editing
" nmap <Leader>ad ea <C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><Esc>o<Esc>
" nmap <Leader>ad ea <C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><Esc>o<Esc>


" It's a MUST for Python code why? will investigate later
au BufRead,BufNewFile *.py setlocal foldmethod=indent       " Improves Python indent
                                                            " Removes need for involking

" Better readability with line wrap
au BufRead,BufNewFile *.md setlocal textwidth=102
" set fo+=t
" set colorcolumn=102

" Autohighlight when passing away more than [XXX] characters
" highlight ColorColumn ctermbg=white
highlight ColorColumn ctermfg=white
call matchadd('ColorColumn', '\%102v', 100)


" Another try at autocompletion
" Plugin 'davidhalter/jedi-vim'
" Plugin 'lifepillar/vim-mucomplete'
" set completeopt+=menuone
" set completeopt+=noselect
" set shortmess+=c   " Shut off completion messages
" set belloff+=ctrlg " If Vim beeps during completion

