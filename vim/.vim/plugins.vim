set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
" ------Plugins------- "
"Plugin 'airblade/vim-gitgutter'"
"Plugin 'colors'"
"Plugin 'davidhalter/jedi-vim'"
"Plugin 'editorconfig/editorconfig-vim'"
"Plugin 'gcmt/breeze.vim'"
"Plugin 'itchyny/lightline.vim'"
"Plugin 'junegunn/fzf'"
"Plugin 'junegunn/fzf.vim'"
"Plugin 'kien/ctrlp.vim'"
"Plugin 'mattn/emmet-vim'"
"Plugin 'scrooloose/nerdtree'"
"Plugin 'SirVer/ultisnips'"
"Plugin 'terryma/vim-multiple-cursors'"
"Plugin 'tomtom/tcomment_vim'"
"Plugin 'tpope/vim-eunuch'"
"Plugin 'tpope/vim-surround'"
"Plugin 'Valloric/YouCompleteMe'"
"Plugin 'vim-airline/vim-airline'"
"Plugin 'vim-airline/vim-airline-themes'"
"Plugin 'VundleVim/Vundle.vim'"
"Plugin 'w0rp/ale'"



Plugin 'VundleVim/Vundle.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'itchyny/lightline.vim'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tpope/vim-eunuch'
Plugin 'tpope/vim-surround'
Plugin 'w0rp/ale'

call vundle#end()
filetype plugin indent on
